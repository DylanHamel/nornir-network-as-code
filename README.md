# Use Nornir for Network As Code

> Dylan Hamel - dylan.hamel@protonmail.com

In this repo you can find an example of "How you can configure your network as code".

You can manage VRF in ``./data/{hostname}/vrfs.yml`` and VLAN in ``./data/{hostname}/vlans.yml``

#### Example with VLAN :

VSP configuration 

![vsp_vlan_01.png](./images/vsp_vlan_01.png)

**Add** a new VLAN (50) in YAML file.

```yaml
vlans:

  - vlan_id: 1
    vlan_name: Default

  - vlan_id: 50									# <<<<<=====
    vlan_name: VLAN-50
    vlan_type: port-mstprstp
    vlan_isid: 100050
    instance_id: 0

  - vlan_id: 100
    vlan_name: VLAN-100
    vlan_type: port-mstprstp
    vlan_isid: 100100
    instance_id: 0
    interface:
      ip: 172.16.100.2
      mask: 255.255.255.0
      vrrp:
        version: 3
        vrrp_id: 1
        ip: 172.16.100.2 
        priority: 100
        enable: True
        preempt: enable
        backup_master: enable
        adver_int: 40
        fast_adv: enable
        fast_adv_int: 200
```

```shell
» ./deploy_vlan.py
```

VLAN 50 is now configured on VSP

![vsp_vlan_02.png](./images/vsp_vlan_02.png)

**Remove** a new VLAN (50-100) in YAML file.

```yaml
vlans:

  - vlan_id: 1
    vlan_name: Default

		# NO 50
		
		# NO 100
		
  - vlan_id: 200
    vlan_name: VLAN-200
    vlan_type: port-mstprstp
    vlan_isid: 200200
    instance_id: 0
    ports:
      - 1/2
      - 1/3
      - 2/1
    interface:
      vrf: vrf001
      ip: 172.16.200.1
      mask: 255.255.255.0
      rsmlt:
        state: enable
        holdup_timer: 9999
```

```shell
» ./deploy_vlan.py
```

![vsp_vlan_03.png](./images/vsp_vlan_03.png)



For the moment it's impossible :

* To exit an VLAN interface of a VRF
* To remove VRRP of a VLAN interface
* To remove RSMLT of a VLAN interface
* To remove an IP address of a VLAN interface
* To change an IP address of a VLAN interface

**All these points are achievable** 



#### Example with VRF :

```yaml
vrfs:

  - vrf_name: GlobalRouter
    vrf_id: 0
    vrf_isid: 3010000

  - vrf_name: vrf001
    vrf_id: 1
    vrf_isid: 3010001
      
  - vrf_name: MgmtRouter
    vrf_id: 512
    vrf_isid: 3010512
```

You can now deploy your configuration.

```bash
» ./deploy_vrf.py
```



If you want add a VRF you only need to add a new object in YAML file. Example to add VRF 500.

```yaml
vrfs:

  - vrf_name: GlobalRouter
    vrf_id: 0
    vrf_isid: 3010000

  - vrf_name: vrf001
    vrf_id: 1
    vrf_isid: 3010001
    
  - vrf_name: vrf500			# <<<<=======
    vrf_id: 500
    vrf_isid: 3010500
      
  - vrf_name: MgmtRouter
    vrf_id: 512
    vrf_isid: 3010512
```

```bash
» ./deploy_vrf.py
```



If you want remove a VRF you need to remove the object in YAML file. Example to remove VRF 1

```yaml
vrfs:

  - vrf_name: GlobalRouter
    vrf_id: 0
    vrf_isid: 3010000
    
  - vrf_name: vrf500
    vrf_id: 500
    vrf_isid: 3010500
      
  - vrf_name: MgmtRouter
    vrf_id: 512
    vrf_isid: 3010512
```

```bash
» ./deploy_vrf.py
```



**In production you need to run this script with a CI/CD Pipeline !**



## Output

#### VLAN

```shell
» ./deploy_vlan.py                                                                        dylan.hamel@MacBook-Pro-de-Dylan
{'sw01': Host: sw01}
get_vars************************************************************************
* sw01 ** changed : False ******************************************************
vvvv get_vars ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Load Host specific datas from ./data/ ** changed : False ------------------ INFO
{ 'vlans': [ {'vlan_id': 1, 'vlan_name': 'Default'},
             { 'instance_id': 0,
               'interface': { 'ip': '172.16.100.2',
                              'mask': '255.255.255.0',
                              'vrrp': { 'adver_int': 40,
                                        'backup_master': 'enable',
                                        'enable': True,
                                        'fast_adv': 'enable',
                                        'fast_adv_int': 200,
                                        'ip': '172.16.100.2',
                                        'preempt': 'enable',
                                        'priority': 100,
                                        'version': 3,
                                        'vrrp_id': 1}},
               'vlan_id': 100,
               'vlan_isid': 100100,
               'vlan_name': 'VLAN-100',
               'vlan_type': 'port-mstprstp'},
             { 'instance_id': 0,
               'interface': { 'ip': '172.16.200.1',
                              'mask': '255.255.255.0',
                              'rsmlt': { 'holdup_timer': 9999,
                                         'state': 'enable'},
                              'vrf': 'vrf001'},
               'ports': ['1/2', '1/3', '2/1'],
               'vlan_id': 200,
               'vlan_isid': 200200,
               'vlan_name': 'VLAN-200',
               'vlan_type': 'port-mstprstp'},
             { 'instance_id': 0,
               'vlan_id': 300,
               'vlan_isid': 300300,
               'vlan_name': 'VLAN-300',
               'vlan_type': 'port-mstprstp'},
             { 'vlan_id': 4001,
               'vlan_name': 'SPMB-1',
               'vlan_type': 'spbm-bvlan'},
             { 'vlan_id': 4002,
               'vlan_name': 'SPMB-2',
               'vlan_type': 'spbm-bvlan'}]}
^^^^ END get_vars ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_check_if_vlanid_already_exists******************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_check_if_vlanid_already_exists ** changed : False vvvvvvvvvvvvvvvvv INFO
---- Execute show vlan basic 1 ** changed : False ------------------------------ INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
1     Default          byPort       0       none         N/A             N/A             0

---- Execute show vlan basic 100 ** changed : False ---------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
100   VLAN-100         byPort       0       none         N/A             N/A             100

---- Execute show vlan basic 200 ** changed : False ---------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
200   VLAN-200         byPort       0       none         N/A             N/A             0

---- Execute show vlan basic 300 ** changed : False ---------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
300   VLAN-300         byPort       0       none         N/A             N/A             0

---- Execute show vlan basic 4001 ** changed : False --------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
4001  SPBM-1           spbm-bvlan   62      none         N/A             N/A             0

---- Execute show vlan basic 4002 ** changed : False --------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
4002  SPBM-2           spbm-bvlan   62      none         N/A             N/A             0

^^^^ END extreme_check_if_vlanid_already_exists ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
generate_template***************************************************************
* sw01 ** changed : False ******************************************************
vvvv generate_template ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Generate templates from ./templates/jinja2/ ** changed : False ------------ INFO
vlan create 100 name VLAN-100 type port-mstprstp 0
vlan i-sid 100 100100
interface vlan 100
ip address 172.16.100.2 255.255.255.0
ip vrrp version 3
ip vrrp address 1 172.16.100.2
ip vrrp 1 enable
ip vrrp 1 priority 100
ip vrrp 1 preempt enable
ip vrrp 1 backup-master enable
ip vrrp 1 adver-int 40
ip vrrp 1 fast-adv-int 200
vlan create 200 name VLAN-200 type port-mstprstp 0
vlan i-sid 200 200200
vlan members 200 1/2,1/3,2/1
interface vlan 200
vrf vrf001
ip address 172.16.200.1 255.255.255.0
ip rsmlt
ip rsmlt holdup-timer 9999
vlan create 300 name VLAN-300 type port-mstprstp 0
vlan i-sid 300 300300
vlan create 4001 name SPMB-1 type spbm-bvlan
vlan create 4002 name SPMB-2 type spbm-bvlan

^^^^ END generate_template ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_deploy_vlan_configuration***********************************************
* sw01 ** changed : True *******************************************************
vvvv extreme_deploy_vlan_configuration ** changed : False vvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute commands in templates/files/sw01_vlan.cfg ** changed : True ------- INFO
config term
Enter configuration commands, one per line.  End with CNTL/Z.
VSP-8284XSQ:1(config)#vlan create 100 name VLAN-100 type port-mstprstp 0

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan i-sid 100 100100

Error: I-SID is already assigned to a VLAN

VSP-8284XSQ:1(config)#interface vlan 100
VSP-8284XSQ:1(config-if)#ip address 172.16.100.2 255.255.255.0

Error: multinetting is not suppported

VSP-8284XSQ:1(config-if)#ip vrrp version 3
VSP-8284XSQ:1(config-if)#ip vrrp address 1 172.16.100.2

Error: RSMLT and VRRP should not be enabled on the same VLAN.

VSP-8284XSQ:1(config-if)#ip vrrp 1 enable

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#ip vrrp 1 priority 100

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#ip vrrp 1 preempt enable

Error: VR of VrId 1 on vlan 100 does not exist

VSP-8284XSQ:1(config-if)#ip vrrp 1 backup-master enable

Error: VR of VrId 1 on vlan 100 does not exist

VSP-8284XSQ:1(config-if)#ip vrrp 1 adver-int 40

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#ip vrrp 1 fast-adv-int 200

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#vlan create 200 name VLAN-200 type port-mstprstp 0

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan i-sid 200 200200

Error: I-SID is already assigned to a VLAN

VSP-8284XSQ:1(config)#vlan members 200 1/2,1/3,2/1
VSP-8284XSQ:1(config)#interface vlan 200
VSP-8284XSQ:1(config-if)#vrf vrf001

Error: Cannot Change VRF association while an Interface exists

VSP-8284XSQ:1(config-if)#ip address 172.16.200.1 255.255.255.0

Error: multinetting is not suppported

VSP-8284XSQ:1(config-if)#ip rsmlt
VSP-8284XSQ:1(config-if)#ip rsmlt holdup-timer 9999
VSP-8284XSQ:1(config-if)#vlan create 300 name VLAN-300 type port-mstprstp 0

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan i-sid 300 300300

Error: I-SID is already assigned to a VLAN

VSP-8284XSQ:1(config)#vlan create 4001 name SPMB-1 type spbm-bvlan

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan create 4002 name SPMB-2 type spbm-bvlan

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#end
VSP-8284XSQ:1#
^^^^ END extreme_deploy_vlan_configuration ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
[['1', 'Default'], ['100', 'VLAN-100'], ['200', 'VLAN-200'], ['300', 'VLAN-300'], ['4001', 'SPBM-1'], ['4002', 'SPBM-2']]
extreme_show_vlan_name**********************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_show_vlan_name ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show vlan name ** changed : False --------------------------------- INFO

====================================================================================================
                                   Vlan Name
====================================================================================================
VLAN  IF
ID    INDEX NAME
----------------------------------------------------------------------------------------------------
1     2049  Default
100   2148  VLAN-100
200   2248  VLAN-200
300   2348  VLAN-300
4001  6049  SPBM-1
4002  6050  SPBM-2

All 6 out of 6 Total Num of Vlan Name Entries displayed

^^^^ END extreme_show_vlan_name ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
verify_all_vlans_exist**********************************************************
* sw01 ** changed : False ******************************************************
vvvv verify_all_vlans_exist ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END verify_all_vlans_exist ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_remove_vlan*************************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_remove_vlan ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END extreme_remove_vlan ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
[['1', 'Default'], ['100', 'VLAN-100'], ['200', 'VLAN-200'], ['300', 'VLAN-300'], ['4001', 'SPBM-1'], ['4002', 'SPBM-2']]
extreme_show_vlan_name**********************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_show_vlan_name ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show vlan name ** changed : False --------------------------------- INFO

====================================================================================================
                                   Vlan Name
====================================================================================================
VLAN  IF
ID    INDEX NAME
----------------------------------------------------------------------------------------------------
1     2049  Default
100   2148  VLAN-100
200   2248  VLAN-200
300   2348  VLAN-300
4001  6049  SPBM-1
4002  6050  SPBM-2

All 6 out of 6 Total Num of Vlan Name Entries displayed

^^^^ END extreme_show_vlan_name ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
{   'sw01': [   {'vlan_id': '1', 'vlan_name': 'Default'},
                {'vlan_id': '100', 'vlan_name': 'VLAN-100'},
                {'vlan_id': '200', 'vlan_name': 'VLAN-200'},
                {'vlan_id': '300', 'vlan_name': 'VLAN-300'},
                {'vlan_id': '4001', 'vlan_name': 'SPBM-1'},
                {'vlan_id': '4002', 'vlan_name': 'SPBM-2'}]}
------------------------------------------------
 main finished...
------------------------------------------------------------
/Volumes/Data/gitlab/nornir-network-as-code(master*) » ./deploy_vlan.py                                                                        dylan.hamel@MacBook-Pro-de-Dylan
{'sw01': Host: sw01}
get_vars************************************************************************
* sw01 ** changed : False ******************************************************
vvvv get_vars ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Load Host specific datas from ./data/ ** changed : False ------------------ INFO
{ 'vlans': [ {'vlan_id': 1, 'vlan_name': 'Default'},
             { 'instance_id': 0,
               'interface': { 'ip': '172.16.100.2',
                              'mask': '255.255.255.0',
                              'vrrp': { 'adver_int': 40,
                                        'backup_master': 'enable',
                                        'enable': True,
                                        'fast_adv': 'enable',
                                        'fast_adv_int': 200,
                                        'ip': '172.16.100.2',
                                        'preempt': 'enable',
                                        'priority': 100,
                                        'version': 3,
                                        'vrrp_id': 1}},
               'vlan_id': 100,
               'vlan_isid': 100100,
               'vlan_name': 'VLAN-100',
               'vlan_type': 'port-mstprstp'},
             { 'instance_id': 0,
               'interface': { 'ip': '172.16.200.1',
                              'mask': '255.255.255.0',
                              'rsmlt': { 'holdup_timer': 9999,
                                         'state': 'enable'},
                              'vrf': 'vrf001'},
               'ports': ['1/2', '1/3', '2/1'],
               'vlan_id': 200,
               'vlan_isid': 200200,
               'vlan_name': 'VLAN-200',
               'vlan_type': 'port-mstprstp'},
             { 'instance_id': 0,
               'vlan_id': 300,
               'vlan_isid': 300300,
               'vlan_name': 'VLAN-300',
               'vlan_type': 'port-mstprstp'},
             { 'vlan_id': 4001,
               'vlan_name': 'SPMB-1',
               'vlan_type': 'spbm-bvlan'},
             { 'vlan_id': 4002,
               'vlan_name': 'SPMB-2',
               'vlan_type': 'spbm-bvlan'}]}
^^^^ END get_vars ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_check_if_vlanid_already_exists******************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_check_if_vlanid_already_exists ** changed : False vvvvvvvvvvvvvvvvv INFO
---- Execute show vlan basic 1 ** changed : False ------------------------------ INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
1     Default          byPort       0       none         N/A             N/A             0

---- Execute show vlan basic 100 ** changed : False ---------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
100   VLAN-100         byPort       0       none         N/A             N/A             100

---- Execute show vlan basic 200 ** changed : False ---------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
200   VLAN-200         byPort       0       none         N/A             N/A             0

---- Execute show vlan basic 300 ** changed : False ---------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
300   VLAN-300         byPort       0       none         N/A             N/A             0

---- Execute show vlan basic 4001 ** changed : False --------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
4001  SPBM-1           spbm-bvlan   62      none         N/A             N/A             0

---- Execute show vlan basic 4002 ** changed : False --------------------------- INFO

==================================================================================================
                                            Vlan Basic
==================================================================================================
VLAN                                MSTP
ID    NAME             TYPE         INST_ID PROTOCOLID   SUBNETADDR      SUBNETMASK      VRFID
--------------------------------------------------------------------------------------------------
4002  SPBM-2           spbm-bvlan   62      none         N/A             N/A             0

^^^^ END extreme_check_if_vlanid_already_exists ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
generate_template***************************************************************
* sw01 ** changed : False ******************************************************
vvvv generate_template ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Generate templates from ./templates/jinja2/ ** changed : False ------------ INFO
vlan create 100 name VLAN-100 type port-mstprstp 0
vlan i-sid 100 100100
interface vlan 100
ip address 172.16.100.2 255.255.255.0
ip vrrp version 3
ip vrrp address 1 172.16.100.2
ip vrrp 1 enable
ip vrrp 1 priority 100
ip vrrp 1 preempt enable
ip vrrp 1 backup-master enable
ip vrrp 1 adver-int 40
ip vrrp 1 fast-adv-int 200
vlan create 200 name VLAN-200 type port-mstprstp 0
vlan i-sid 200 200200
vlan members 200 1/2,1/3,2/1
interface vlan 200
vrf vrf001
ip address 172.16.200.1 255.255.255.0
ip rsmlt
ip rsmlt holdup-timer 9999
vlan create 300 name VLAN-300 type port-mstprstp 0
vlan i-sid 300 300300
vlan create 4001 name SPMB-1 type spbm-bvlan
vlan create 4002 name SPMB-2 type spbm-bvlan

^^^^ END generate_template ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_deploy_vlan_configuration***********************************************
* sw01 ** changed : True *******************************************************
vvvv extreme_deploy_vlan_configuration ** changed : False vvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute commands in templates/files/sw01_vlan.cfg ** changed : True ------- INFO
config term
Enter configuration commands, one per line.  End with CNTL/Z.
VSP-8284XSQ:1(config)#vlan create 100 name VLAN-100 type port-mstprstp 0

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan i-sid 100 100100

Error: I-SID is already assigned to a VLAN

VSP-8284XSQ:1(config)#interface vlan 100
VSP-8284XSQ:1(config-if)#ip address 172.16.100.2 255.255.255.0

Error: multinetting is not suppported

VSP-8284XSQ:1(config-if)#ip vrrp version 3
VSP-8284XSQ:1(config-if)#ip vrrp address 1 172.16.100.2

Error: RSMLT and VRRP should not be enabled on the same VLAN.

VSP-8284XSQ:1(config-if)#ip vrrp 1 enable

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#ip vrrp 1 priority 100

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#ip vrrp 1 preempt enable

Error: VR of VrId 1 on vlan 100 does not exist

VSP-8284XSQ:1(config-if)#ip vrrp 1 backup-master enable

Error: VR of VrId 1 on vlan 100 does not exist

VSP-8284XSQ:1(config-if)#ip vrrp 1 adver-int 40

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#ip vrrp 1 fast-adv-int 200

Error: Can't find VRRP entry with specified IfIndex and VrId.

VSP-8284XSQ:1(config-if)#vlan create 200 name VLAN-200 type port-mstprstp 0

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan i-sid 200 200200

Error: I-SID is already assigned to a VLAN

VSP-8284XSQ:1(config)#vlan members 200 1/2,1/3,2/1
VSP-8284XSQ:1(config)#interface vlan 200
VSP-8284XSQ:1(config-if)#vrf vrf001

Error: Cannot Change VRF association while an Interface exists

VSP-8284XSQ:1(config-if)#ip address 172.16.200.1 255.255.255.0

Error: multinetting is not suppported

VSP-8284XSQ:1(config-if)#ip rsmlt
VSP-8284XSQ:1(config-if)#ip rsmlt holdup-timer 9999
VSP-8284XSQ:1(config-if)#vlan create 300 name VLAN-300 type port-mstprstp 0

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan i-sid 300 300300

Error: I-SID is already assigned to a VLAN

VSP-8284XSQ:1(config)#vlan create 4001 name SPMB-1 type spbm-bvlan

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#vlan create 4002 name SPMB-2 type spbm-bvlan

Error: The specified VLAN ID is invalid or already in use

VSP-8284XSQ:1(config)#end
VSP-8284XSQ:1#
^^^^ END extreme_deploy_vlan_configuration ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
[['1', 'Default'], ['99', 'tempo2'], ['100', 'VLAN-100'], ['200', 'VLAN-200'], ['300', 'VLAN-300'], ['999', 'tempo'], ['4001', 'SPBM-1'], ['4002', 'SPBM-2']]
extreme_show_vlan_name**********************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_show_vlan_name ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show vlan name ** changed : False --------------------------------- INFO

====================================================================================================
                                   Vlan Name
====================================================================================================
VLAN  IF
ID    INDEX NAME
----------------------------------------------------------------------------------------------------
1     2049  Default
99    2147  tempo2
100   2148  VLAN-100
200   2248  VLAN-200
300   2348  VLAN-300
999   3047  tempo
4001  6049  SPBM-1
4002  6050  SPBM-2

All 8 out of 8 Total Num of Vlan Name Entries displayed

^^^^ END extreme_show_vlan_name ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
verify_all_vlans_exist**********************************************************
* sw01 ** changed : False ******************************************************
vvvv verify_all_vlans_exist ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END verify_all_vlans_exist ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_remove_vlan*************************************************************
* sw01 ** changed : True *******************************************************
vvvv extreme_remove_vlan ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Generate templates from ./templates/jinja2/remove_vlan.j2 ** changed : False  INFO
vlan delete 99
vlan delete 999

---- Remove vlan ** changed : True --------------------------------------------- INFO
config term
Enter configuration commands, one per line.  End with CNTL/Z.
VSP-8284XSQ:1(config)#vlan delete 99
VSP-8284XSQ:1(config)#vlan delete 999
VSP-8284XSQ:1(config)#end
VSP-8284XSQ:1#
^^^^ END extreme_remove_vlan ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
[['1', 'Default'], ['100', 'VLAN-100'], ['200', 'VLAN-200'], ['300', 'VLAN-300'], ['4001', 'SPBM-1'], ['4002', 'SPBM-2']]
extreme_show_vlan_name**********************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_show_vlan_name ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show vlan name ** changed : False --------------------------------- INFO

====================================================================================================
                                   Vlan Name
====================================================================================================
VLAN  IF
ID    INDEX NAME
----------------------------------------------------------------------------------------------------
1     2049  Default
100   2148  VLAN-100
200   2248  VLAN-200
300   2348  VLAN-300
4001  6049  SPBM-1
4002  6050  SPBM-2

All 6 out of 6 Total Num of Vlan Name Entries displayed

^^^^ END extreme_show_vlan_name ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
{   'sw01': [   {'vlan_id': '1', 'vlan_name': 'Default'},
                {'vlan_id': '100', 'vlan_name': 'VLAN-100'},
                {'vlan_id': '200', 'vlan_name': 'VLAN-200'},
                {'vlan_id': '300', 'vlan_name': 'VLAN-300'},
                {'vlan_id': '4001', 'vlan_name': 'SPBM-1'},
                {'vlan_id': '4002', 'vlan_name': 'SPBM-2'}]}
------------------------------------------------
 main finished...
```



#### VRF 

```bash
» ./deploy_vrf.py
{'sw01': Host: sw01, 'sw02': Host: sw02}
get_vars************************************************************************
* sw01 ** changed : False ******************************************************
vvvv get_vars ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Load Host specific datas from ./data/ ** changed : False ------------------ INFO
{ 'vrfs': [ {'vrf_id': 0, 'vrf_isid': 3010000, 'vrf_name': 'GlobalRouter'},
            {'vrf_id': 1, 'vrf_isid': 3010001, 'vrf_name': 'vrf001'},
            {'vrf_id': 2, 'vrf_isid': 3010002, 'vrf_name': 'vrf002'},
            {'vrf_id': 3, 'vrf_isid': 3010003, 'vrf_name': 'vrf003'},
            {'vrf_id': 111, 'vrf_isid': 3010111, 'vrf_name': 'vrf111'},
            {'vrf_id': 512, 'vrf_isid': 3010512, 'vrf_name': 'MgmtRouter'}]}
^^^^ END get_vars ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv get_vars ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Load Host specific datas from ./data/ ** changed : False ------------------ INFO
{ 'vrfs': [ {'vrf_id': 0, 'vrf_isid': 3010000, 'vrf_name': 'GlobalRouter'},
            {'vrf_id': 1, 'vrf_isid': 3010001, 'vrf_name': 'vrf001'},
            {'vrf_id': 2, 'vrf_isid': 3010002, 'vrf_name': 'vrf002'},
            {'vrf_id': 3, 'vrf_isid': 3010003, 'vrf_name': 'vrf003'},
            {'vrf_id': 222, 'vrf_isid': 3010222, 'vrf_name': 'vrf222'},
            {'vrf_id': 512, 'vrf_isid': 3010512, 'vrf_name': 'MgmtRouter'}]}
^^^^ END get_vars ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_check_if_vrfid_already_exists*******************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_check_if_vrfid_already_exists ** changed : False vvvvvvvvvvvvvvvvvv INFO
---- Execute show ip vrf vrfid 0 ** changed : False ---------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
5          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
GlobalRouter              0    TRUE    TRUE    TRUE    TRUE    3       2       TRUE

1 out of 5 Total Num of VRF Entries displayed.
---- Execute show ip vrf vrfid 1 ** changed : False ---------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
5          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
vrf001                    1    FALSE   FALSE   FALSE   FALSE   0       0       FALSE

1 out of 5 Total Num of VRF Entries displayed.
---- Execute show ip vrf vrfid 2 ** changed : False ---------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
5          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
vrf002                    2    FALSE   FALSE   FALSE   FALSE   0       0       FALSE

1 out of 5 Total Num of VRF Entries displayed.
---- Execute show ip vrf vrfid 3 ** changed : False ---------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
5          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
vrf003                    3    FALSE   FALSE   FALSE   FALSE   0       0       FALSE

1 out of 5 Total Num of VRF Entries displayed.
---- Execute show ip vrf vrfid 111 ** changed : False -------------------------- INFO

Info: No VRF exists with id: 111
---- Execute show ip vrf vrfid 512 ** changed : False -------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
5          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
MgmtRouter                512  FALSE   FALSE   FALSE   FALSE   1       3       FALSE

1 out of 5 Total Num of VRF Entries displayed.
^^^^ END extreme_check_if_vrfid_already_exists ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv extreme_check_if_vrfid_already_exists ** changed : False vvvvvvvvvvvvvvvvvv INFO
---- Execute show ip vrf vrfid 0 ** changed : False ---------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
2          1          1          1          1          4          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
GlobalRouter              0    TRUE    TRUE    TRUE    TRUE    1       0       TRUE

1 out of 2 Total Num of VRF Entries displayed.
---- Execute show ip vrf vrfid 1 ** changed : False ---------------------------- INFO

Info: No VRF exists with id: 1
---- Execute show ip vrf vrfid 2 ** changed : False ---------------------------- INFO

Info: No VRF exists with id: 2
---- Execute show ip vrf vrfid 3 ** changed : False ---------------------------- INFO

Info: No VRF exists with id: 3
---- Execute show ip vrf vrfid 222 ** changed : False -------------------------- INFO

Info: No VRF exists with id: 222
---- Execute show ip vrf vrfid 512 ** changed : False -------------------------- INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
2          1          1          1          1          4          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
MgmtRouter                512  FALSE   FALSE   FALSE   FALSE   1       4       FALSE

1 out of 2 Total Num of VRF Entries displayed.
^^^^ END extreme_check_if_vrfid_already_exists ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
generate_template***************************************************************
* sw01 ** changed : False ******************************************************
vvvv generate_template ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Generate templates from ./templates/jinja2/ ** changed : False ------------ INFO
ip vrf vrf111 vrfid 111

router vrf vrf111
exit

router vrf vrf111
ipvpn
i-sid 3010111
ipvpn enable
exit

router vrf vrf111
isis redistribute direct
isis redistribute direct enable
exit

^^^^ END generate_template ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv generate_template ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Generate templates from ./templates/jinja2/ ** changed : False ------------ INFO
ip vrf vrf001 vrfid 1

router vrf vrf001
exit

router vrf vrf001
ipvpn
i-sid 3010001
ipvpn enable
exit

router vrf vrf001
isis redistribute direct
isis redistribute direct enable
exit
ip vrf vrf002 vrfid 2

router vrf vrf002
exit

router vrf vrf002
ipvpn
i-sid 3010002
ipvpn enable
exit

router vrf vrf002
isis redistribute direct
isis redistribute direct enable
exit
ip vrf vrf003 vrfid 3

router vrf vrf003
exit

router vrf vrf003
ipvpn
i-sid 3010003
ipvpn enable
exit

router vrf vrf003
isis redistribute direct
isis redistribute direct enable
exit
ip vrf vrf222 vrfid 222

router vrf vrf222
exit

router vrf vrf222
ipvpn
i-sid 3010222
ipvpn enable
exit

router vrf vrf222
isis redistribute direct
isis redistribute direct enable
exit

^^^^ END generate_template ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_configure_new_vrf*******************************************************
* sw01 ** changed : True *******************************************************
vvvv extreme_configure_new_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute commands in templates/files/sw01_vrf.cfg ** changed : True -------- INFO
config term
Enter configuration commands, one per line.  End with CNTL/Z.
VSP-8284XSQ:1(config)#ip vrf vrf111 vrfid 111
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf111
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf111
VSP-8284XSQ:1(router-vrf)#ipvpn
VSP-8284XSQ:1(router-vrf)#i-sid 3010111
VSP-8284XSQ:1(router-vrf)#ipvpn enable
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf111
VSP-8284XSQ:1(router-vrf)#isis redistribute direct
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#isis redistribute direct enable
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#end
VSP-8284XSQ:1#
^^^^ END extreme_configure_new_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : True *******************************************************
vvvv extreme_configure_new_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute commands in templates/files/sw02_vrf.cfg ** changed : True -------- INFO
config term
Enter configuration commands, one per line.  End with CNTL/Z.
VSP-8284XSQ:1(config)#ip vrf vrf001 vrfid 1
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf001
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf001
VSP-8284XSQ:1(router-vrf)#ipvpn
VSP-8284XSQ:1(router-vrf)#i-sid 3010001
VSP-8284XSQ:1(router-vrf)#ipvpn enable
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf001
VSP-8284XSQ:1(router-vrf)#isis redistribute direct
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#isis redistribute direct enable
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#ip vrf vrf002 vrfid 2
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf002
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf002
VSP-8284XSQ:1(router-vrf)#ipvpn
VSP-8284XSQ:1(router-vrf)#i-sid 3010002
VSP-8284XSQ:1(router-vrf)#ipvpn enable
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf002
VSP-8284XSQ:1(router-vrf)#isis redistribute direct
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#isis redistribute direct enable
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#ip vrf vrf003 vrfid 3
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf003
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf003
VSP-8284XSQ:1(router-vrf)#ipvpn
VSP-8284XSQ:1(router-vrf)#i-sid 3010003
VSP-8284XSQ:1(router-vrf)#ipvpn enable
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf003
VSP-8284XSQ:1(router-vrf)#isis redistribute direct
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#isis redistribute direct enable
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#ip vrf vrf222 vrfid 222
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf222
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf222
VSP-8284XSQ:1(router-vrf)#ipvpn
VSP-8284XSQ:1(router-vrf)#i-sid 3010222
VSP-8284XSQ:1(router-vrf)#ipvpn enable
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#
VSP-8284XSQ:1(config)#router vrf vrf222
VSP-8284XSQ:1(router-vrf)#isis redistribute direct
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#isis redistribute direct enable
Error: Cannot configure isis redistribution when SPBM is disabled
VSP-8284XSQ:1(router-vrf)#exit
VSP-8284XSQ:1(config)#end
VSP-8284XSQ:1#
^^^^ END extreme_configure_new_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_show_ip_vrf*************************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_show_ip_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show ip vrf ** changed : False ------------------------------------ INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
6          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
GlobalRouter              0    TRUE    TRUE    TRUE    TRUE    3       2       TRUE
vrf001                    1    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf002                    2    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf003                    3    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf111                    111  FALSE   FALSE   FALSE   FALSE   0       0       FALSE
MgmtRouter                512  FALSE   FALSE   FALSE   FALSE   1       3       FALSE

6 out of 6 Total Num of VRF Entries displayed.
^^^^ END extreme_show_ip_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv extreme_show_ip_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show ip vrf ** changed : False ------------------------------------ INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
6          1          1          1          1          4          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
GlobalRouter              0    TRUE    TRUE    TRUE    TRUE    1       0       TRUE
vrf001                    1    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf002                    2    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf003                    3    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf222                    222  FALSE   FALSE   FALSE   FALSE   0       0       FALSE
MgmtRouter                512  FALSE   FALSE   FALSE   FALSE   1       4       FALSE

6 out of 6 Total Num of VRF Entries displayed.
^^^^ END extreme_show_ip_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
verify_all_vlans_exist**********************************************************
* sw01 ** changed : False ******************************************************
vvvv verify_all_vlans_exist ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END verify_all_vlans_exist ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv verify_all_vlans_exist ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END verify_all_vlans_exist ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
[extreme_remove_vrf] - []
[extreme_remove_vrf] - []
extreme_remove_vrf**************************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_remove_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END extreme_remove_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv extreme_remove_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
^^^^ END extreme_remove_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
extreme_show_ip_vrf*************************************************************
* sw01 ** changed : False ******************************************************
vvvv extreme_show_ip_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show ip vrf ** changed : False ------------------------------------ INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
6          1          1          1          1          5          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
GlobalRouter              0    TRUE    TRUE    TRUE    TRUE    3       2       TRUE
vrf001                    1    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf002                    2    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf003                    3    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf111                    111  FALSE   FALSE   FALSE   FALSE   0       0       FALSE
MgmtRouter                512  FALSE   FALSE   FALSE   FALSE   1       3       FALSE

6 out of 6 Total Num of VRF Entries displayed.
^^^^ END extreme_show_ip_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* sw02 ** changed : False ******************************************************
vvvv extreme_show_ip_vrf ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
---- Execute show ip vrf ** changed : False ------------------------------------ INFO


====================================================================================================
                                VRF INFORMATION
====================================================================================================
VRF        OSPF       RIP        BGP        PIM        ARP        PIM6
COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
----------------------------------------------------------------------------------------------------
6          1          1          1          1          4          1

VRF                       VRF                                  VLAN    ARP
NAME                      ID   OSPF    RIP     BGP     PIM     COUNT   COUNT   PIM6
----------------------------------------------------------------------------------------------------
GlobalRouter              0    TRUE    TRUE    TRUE    TRUE    1       0       TRUE
vrf001                    1    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf002                    2    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf003                    3    FALSE   FALSE   FALSE   FALSE   0       0       FALSE
vrf222                    222  FALSE   FALSE   FALSE   FALSE   0       0       FALSE
MgmtRouter                512  FALSE   FALSE   FALSE   FALSE   1       4       FALSE

6 out of 6 Total Num of VRF Entries displayed.
^^^^ END extreme_show_ip_vrf ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
{   'sw01': [   {   'arp_count': '2',
                    'vlan_count': '3',
                    'vrf_id': '0',
                    'vrf_name': 'GlobalRouter'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '1',
                    'vrf_name': 'vrf001'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '2',
                    'vrf_name': 'vrf002'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '3',
                    'vrf_name': 'vrf003'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '111',
                    'vrf_name': 'vrf111'},
                {   'arp_count': '3',
                    'vlan_count': '1',
                    'vrf_id': '512',
                    'vrf_name': 'MgmtRouter'}],
    'sw02': [   {   'arp_count': '0',
                    'vlan_count': '1',
                    'vrf_id': '0',
                    'vrf_name': 'GlobalRouter'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '1',
                    'vrf_name': 'vrf001'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '2',
                    'vrf_name': 'vrf002'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '3',
                    'vrf_name': 'vrf003'},
                {   'arp_count': '0',
                    'vlan_count': '0',
                    'vrf_id': '222',
                    'vrf_name': 'vrf222'},
                {   'arp_count': '4',
                    'vlan_count': '1',
                    'vrf_id': '512',
                    'vrf_name': 'MgmtRouter'}]}
------------------------------------------------
 main finished...
------------------------------------------------------------
```

