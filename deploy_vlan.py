#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"
__license__ = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from os import mkdir
except ImportError as importError:
    print("Error import [deploy_vlan.py] mkdir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import yaml
except ImportError as importError:
    print("Error import [deploy_vlan.py] yaml")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pdb
except ImportError as importError:
    print("Error import [deploy_vlan.py] pdb")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [deploy_vlan.py] pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import textfsm
except ImportError as importError:
    print("Error import [deploy_vrf.py] textfsm")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir import InitNornir

# To use advanced filters
    from nornir.core.filter import F

# To use HTTP requests
    from nornir.plugins.tasks.apis import http_method

# To execute commands through SSH
    from nornir.plugins.tasks.commands import remote_command

# Netmiko commands
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.plugins.tasks.networking import netmiko_save_config
    from nornir.plugins.tasks.networking import netmiko_send_config

# To use PING
    from nornir.plugins.tasks.networking import tcp_ping

# To print task results
    from nornir.plugins.functions.text import print_result

# To retrieve device datas in a YAML file
    from nornir.plugins.tasks.data import load_yaml

    # To generate template from Jinja2
    from nornir.plugins.tasks.text import template_file

except ImportError as importError:
    print("Error import [deploy_vlan.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
VLAN_NO_EXISTS = "invalid vlan id"

######################################################
#
# Variables
#
nr = InitNornir(
    config_file="./nornir/config.yml",
    logging={"file": "./nornir/nornir.log", "level": "debug"}
)

result = dict()
vlan_missing = dict()
vlan_to_remove = dict()
######################################################
#
# Functions
#

# ----------------------------------------------------------------------
#
#
def get_vars(task):
    
    output = task.run(
            name="Load Host specific datas from ./data/",
            task=load_yaml,
            file=f"./data/{task.host}/vlans.yml",
        )
        
    task.host["vars"] = output.result

# ----------------------------------------------------------------------
#
#
def create_yaml_file_backup(task):

    ## Create a Backup.
    try:
        mkdir(f"./data/backup/")
    except FileExistsError:
        pass

    try:
        mkdir(f"./data/backup/{task.host}")
    except FileExistsError:
        pass

    with open(f"./data/backup/{task.host}/vlans.yml", 'w') as outfile:
        yaml.dump(task.host["vars"], outfile, default_flow_style=False)

# ----------------------------------------------------------------------
#
#
def check_diff_between_yaml_data_file(task):

    import difflib

    ## Create a compate.
    try:
        mkdir(f"./data/compare/")
    except FileExistsError:
        pass

    try:
        mkdir(f"./data/compare/{task.host}")
    except FileExistsError:
        pass

    with open(f"./data/compare/{task.host}/vlans.yml", 'w') as outfile:
        yaml.dump(task.host["vars"], outfile, default_flow_style=False)

    with open(f'./data/compare/{task.host}/vlans.yml') as f2:
        f1_text = f2.read()
    with open(f'./data/backup/{task.host}/vlans.yml') as f2:
        f2_text = f2.read()

    # Find and print the diff:
    for line in difflib.unified_diff(f2_text.strip().splitlines(), f1_text.strip().splitlines(), fromfile='file1', tofile='file2', lineterm='\n'):
        print(line)


#
# Generate template From Jinja2
#
def generate_template(task):
    
    output = task.run(
        name="Generate templates from ./templates/jinja2/",
        task=template_file,
        template="deploy_vlan.j2",
        path="./templates/jinja2/"
    )

    file = open(f"./templates/files/{task.host.name}_vlan.cfg", "w+")
    file.write(output.result)
    file.close()

#
# NETWORK DEVICES - NETMIKO
#


def extreme_check_if_vlanid_already_exists(task):

	task.host["vlan_to_create"] = list()
	task.host["vlan_in_code"] = list()

	for vlan in task.host['vars']['vlans']:

		output = task.run(
			name=f"Execute show vlan basic {vlan['vlan_id']}",
			task=netmiko_send_command,
			command_string=f"show vlan basic {vlan['vlan_id']}",
			enable=True
		)

		if VLAN_NO_EXISTS in output.result:
			task.host["vlan_to_create"].append(vlan)

		task.host["vlan_in_code"].append(vlan)

# ----------------------------------------------------
#
#


def extreme_deploy_vlan_configuration(task):

    if "vars" in task.host.keys() and len(task.host['vars']) > 0:
        output = task.run(
            name=f"Execute commands in templates/files/{task.host.name}_vlan.cfg",
            task=netmiko_send_config,
            config_file=f"./templates/files/{task.host.name}_vlan.cfg"
        )

# ----------------------------------------------------
#
#

def extreme_show_vlan_name(task):

    output = task.run(
        name="Execute show vlan name",
        task=netmiko_send_command,
        command_string="show vlan name",
        enable=True
    )

    template = open(
        'templates/textfsm/extreme_vsp_show_vlan_name.template')
    results_template = textfsm.TextFSM(template)

    parsed_results = results_template.ParseText(output.result)
    result[task.host.name] = list()

    print(parsed_results)

    for line in parsed_results:
        tmp = dict()
        tmp["vlan_id"] = line[0]
        tmp["vlan_name"] = line[1]
        result[task.host.name].append(tmp)

# ----------------------------------------------------
#
#

def verify_all_vlans_exist(task):

    lst_vlan_ids_in_config = extract_vlanid_from_output(result[task.host.name])
    lst_vlan_ids_in_code = extract_vlanid_from_output(task.host['vlan_in_code'])

    vlan_missing = list()

    for vrf_in_conf in lst_vlan_ids_in_config:
        if str(vrf_in_conf) not in lst_vlan_ids_in_code:
            vlan_missing.append(vrf_in_conf)

    task.host["vlan_to_remove"] = vlan_missing

# ----------------------------------------------------
#
#

def extreme_remove_vlan(task):

    if "vlan_to_remove" in task.host.keys() and len(task.host["vlan_to_remove"]) > 0:

        output = task.run(
            name="Generate templates from ./templates/jinja2/remove_vlan.j2",
            task=template_file,
            template="remove_vlan.j2",
            path="./templates/jinja2/"
        )

        output = task.run(
            name="Remove vlan",
            task=netmiko_send_config,
            config_commands=output.result
        )

# ----------------------------------------------------
#
#

def extract_vlanid_from_output(show_vlan_name_output: list()) -> list():
    
    lst_vlan_ids = list()
    
    for vlan_infos in show_vlan_name_output:
        lst_vlan_ids.append(str(vlan_infos['vlan_id']))
        
    return lst_vlan_ids

######################################################
#
# MAIN Functions
#


def main():

    devices = nr.filter(F(groups__contains="switch"))
    print(devices.inventory.hosts)

    # print(devices.inventory.hosts['sw01'].platform)
    # devices.inventory.add_host("sw02")

    # Retrieve vars
    data = devices.run(
        task=get_vars,
        on_failed=True
    )
    print_result(data)

    # Compare files
    data = devices.run(
        task=check_diff_between_yaml_data_file
    )
    print_result(data)
    
    #Check if VRF already exists on VSP
    data = devices.run(
        task=extreme_check_if_vlanid_already_exists,
        on_failed=True
    )
    print_result(data)

    # Generate config from Jinja2
    data = devices.run(
        task=generate_template,
        on_failed=True
    )
    print_result(data)
    
    # Deploy configuration on VSP
    data = devices.run(
        task=extreme_deploy_vlan_configuration,
        on_failed=True
    )
    print_result(data)

    
    # Show all VLAN on devices
    data = devices.run(
        task=extreme_show_vlan_name,
        on_failed=True
    )
    print_result(data)

    
    # Show all VRFs on devices
    data = devices.run(
        task=verify_all_vlans_exist,
        on_failed=True
    )
    print_result(data)
    
    
    # Remove VRFs that are not in yaml
    data = devices.run(
        task=extreme_remove_vlan,
        on_failed=True
    )
    print_result(data)

    # Show all VLANs on devices
    data = devices.run(
        task=extreme_show_vlan_name,
        on_failed=True
    )
    print_result(data)

    # Create Backup
    data = devices.run(
        task=create_yaml_file_backup
    )
    print_result(data)
    
    PP.pprint(result)
    
    import pdb
    pdb.set_trace()

    print("------------------------------------------------ \n main finished...")
    exit(EXIT_SUCCESS)



# ----------------------------------------------------
#
#
if __name__ == "__main__":
    main()
